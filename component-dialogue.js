/* global AFRAME */
var dialogueTextEl;
var helpMenuActive = true;
var index = 0;
var lines;
var speakerPlateEl;
var speakerTextEl;
var numberOfLines = 45; // TODO: Update in Init via REST

// Get Dialogue from Line Index
function getDialogue(index) {
  return lines[index].dialogue
}

// Get Speaker from Line Index
function getSpeaker(index) {
  return lines[index].speaker
}

// Shows or Hides Speaker Plate
function setSpeakerPlateVisibility(speaker) {
  if (speaker){
    speakerPlateEl.object3D.visible = true;
  }
  else {
    speakerPlateEl.object3D.visible = false;
  }
}

AFRAME.registerComponent('component-dialogue', {
  init: function () {

    // Add Event Listen for Keystokes and Parse in onKeydown()
    window.addEventListener('keydown', this.onKeydown);

    lines = [
      {speaker: "", dialogue: "Press N to go to Next Page and Press B for the Previous Page"},
      {speaker: "", dialogue: "I dream of the next step."},
      {speaker: "", dialogue: "My personal salvation."},
      {speaker: "", dialogue: "The answer to my waiting and wandering."},
      {speaker: "", dialogue: "In my dream, it’s so close."},
      {speaker: "", dialogue: "I can almost reach out and grab it."},
      {speaker: "", dialogue: "As beautiful as the stars in the night sky, I turn around and reach out my hand, almost close enough to grab onto them-"}
      ]

    // Reset vars if the users decided to act like goofy goobers
    index = 0
    numberOfLines = lines.length
    console.log("---Number of Lines Imported: " + numberOfLines)

    // Set first line of dialogue and speaker as the help info
    // TODO

    // Get AFrame entities by ids that we are going to update
    dialogueTextEl = document.querySelector('#dialogueText')
    speakerPlateEl = document.querySelector('#speakerPlate');
    speakerTextEl = document.querySelector('#speakerText');

    // this.backgroundEl = document.querySelector('#background');
    // var buttonEls = document.querySelectorAll('.menu-button');
    // this.movieImageEl;
    // this.movieTitleEl = document.querySelector('#movieTitle');
    // this.movieDescriptionEl = document.querySelector('#movieDescription');

    // this.onMenuButtonClick = this.onMenuButtonClick.bind(this);
    // for (var i = 0; i < buttonEls.length; ++i) {
    //   buttonEls[i].addEventListener('click', this.onMenuButtonClick);
    // }
  },

  // onMenuButtonClick: function (evt) {
    // var movieInfo = this.movieInfo[evt.currentTarget.id];

    // this.backgroundEl.object3D.scale.set(1, 1, 1);

    // this.el.object3D.scale.set(1, 1, 1);
    // if (AFRAME.utils.device.isMobile()) { this.el.object3D.scale.set(1.4, 1.4, 1.4); }
    // this.el.object3D.visible = true;
    // this.fadeBackgroundEl.object3D.visible = true;

    // if (this.movieImageEl) { this.movieImageEl.object3D.visible = false; }
    // this.movieImageEl = movieInfo.imgEl;
    // this.movieImageEl.object3D.visible = true;

    // this.movieTitleEl.setAttribute('text', 'value', movieInfo.title);
    // this.movieDescriptionEl.setAttribute('text', 'value', movieInfo.description);
  // },

  onKeydown: function (evt) {
    let code = evt.code

    // (Help) Open/close the help menu
    // TODO: Create Help Menu
    // TODO: Add logic to flip help menu visibiliy 
    if (code === 'KeyH') { 
      console.log("Opening/closing help menu")
    }

    // (Next) Go to next line of dialogue ()
    if (code === 'KeyN') { 
      // Do not go over maximum amount of lines 
      if ((index + 1) <= numberOfLines){
        index++
      }

      // Hide speaker plate if character is monologuing
      let speaker = getSpeaker(index)
      setSpeakerPlateVisibility(speaker)

      // Set text values for speaker and dialogue
      speakerTextEl.setAttribute('text', 'value', speaker);
      dialogueTextEl.setAttribute('text', 'value', getDialogue(index));
    }

    // (Back) Go to previous line of dialogue 
    if (code === 'KeyB') { 
      // Do not go before the first line of dialogue
      if ((index - 1) >= 0){
        index--
      }

      // Hide speaker plate if character is monologuing
      let speaker = getSpeaker(index)
      setSpeakerPlateVisibility(speaker)
 
      // Set text values for speaker and dialogue
      speakerTextEl.setAttribute('text', 'value', speaker);
      dialogueTextEl.setAttribute('text', 'value', getDialogue(index));
    }
  }
});
