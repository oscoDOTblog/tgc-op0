/* global AFRAME */
var dialogueTextEl;
var previousTime = new Date()
var secondsElapsed = 0;

// Object Generator 
var booleanGenerateCubes = false; // default: false
var cubeEntities = []
var cubeLimit = 100
var cubeSpawnDirection = 1
var cubeSpawnX = -100
var cubeSpawnY = 0
var cubeSpawnZ = -100
var cubeVelocityZ = 0.01

// Transition Variables
var currentTexture = 1;
var transitioning = false;
var transitionDuration = 2000; // Transition duration in milliseconds
var blackColor = '#000000';
// var transitionBeep1Beep2 = false

// -- Trigger Variables (can only be updated once) -- //
// Beeps
var beep1Boolean = false
var beep1Timer = 2500;
var beep2Boolean = false;
var beep2Timer = 5500;
var beep3Boolean = false;
var beep3Timer = 8000;
var beep4Boolean = false;
var beep4Timer = 11000;
var beep1Boolean = false
// Portals
var portal1Boolean = false;
var portal1Timer = 14000;
var portal2Boolean = false;
var portal2Timer = 16500;
var portal3Boolean = false;
var portal3Timer = 19500;
var portal4Boolean = false;
var portal4Timer = 22500;

// -- Asset URLs (In Order of Usage) -- //
// Beeps
var beep1Skybox = "#skyboxBeep1"
var beep2Skybox = "#skyboxBeep2"
var beep3Skybox = "#skyboxBeep3"
var beep4Skybox = "#skyboxBeep4"
// Portals
var portal1Skybox = "#skyboxPortal1"
var portal2Skybox = "#skyboxPortal2"
var portal3Skybox = "#skyboxPortal3"
var portal4Skybox = "#skyboxPortal4"

// Transition Functions
function transitionSkybox(skyEl, nextTexture) {
    skyEl.setAttribute('material', 'opacity', 1);
    setTimeout(() => {
      fadeIn(skyEl, nextTexture);
    }, transitionDuration);
}
function fadeIn(skyEl, texture) {
  skyEl.setAttribute('animation__fade', {
    property: 'material.opacity',
    startEvents: 'transitionStart',
    from: 0,
    to: 1,
    dur: transitionDuration, 
    easing: 'linear'
  });
  skyEl.emit('transitionStart');
  skyEl.setAttribute('src', texture);
}

function fadeOut(skyEl){
  skyEl.setAttribute('animation__fade', {
    property: 'material.opacity',
    startEvents: 'transitionStart',
    from: 1,
    to: 0,
    dur: transitionDuration, // Half of transition duration for fade out
    easing: 'linear'
  });
  skyEl.emit('transitionStart');
}

function generateCubes(scene, timeDelta) {
  // Generate the cube position in front of the player
    // Generate the random cube position
  // var spawnX = Math.random() * 200 - 100; // Random float between -10 and 10 for X position
  var spawnX = cubeSpawnX + cubeSpawnDirection;
  console.log(spawnX)
  cubeSpawnX = spawnX
  if (cubeSpawnX == 100) {
    cubeSpawnDirection = -1; // Change direction to decrement
  } else if (cubeSpawnX == -100) {
    cubeSpawnDirection = 1; // Change direction to increment
  }
  var spawnY = 0; // Y position remains 0
  var spawnZ = cubeSpawnZ; // Z position remains -100
  var spawnPosition = {x: spawnX, y: spawnY, z: spawnZ}
  console.log(spawnPosition)

  // Create a new cube entity
  var cubeEntity = document.createElement("a-entity");
  cubeEntity.setAttribute("geometry", "primitive: box");
  cubeEntity.setAttribute("material", "color: white");
  cubeEntity.setAttribute("position", spawnPosition);

  // Add cube entity to the cubeEntities array
  if (cubeEntities.length < cubeLimit){
    cubeEntities.push({
      entity: cubeEntity,
      timeAlive: 0 // Record the spawn time of the cube
    });
    // Append the cube to the scene
    scene.appendChild(cubeEntity);
  }

  // Loop through each cube entity in the array
  for (var i = 0; i < cubeEntities.length; i++) {
    var cube = cubeEntities[i];
    console.log(cube)

    // Calculate the lifespan of the cube
    var lifespan = timeDelta + cube.timeAlive;

    // If the lifespan exceeds 2000 milliseconds, remove the cube entity from the scene
    if (lifespan > 2000) {
      scene.removeChild(cube.entity);
      // Remove the cube entity from the cubeEntities array
      cubeEntities.splice(i, 1);
      i--; // Decrement the index as the array size decreases
    }
    else {
      cube.timeAlive = lifespan
      var positionAttr = cube.entity.getAttribute("position");
      var updatedX = Math.random() * 20 - 10; // Random float between -100 and 100 for X position
      var updatedY = 0
      var updatedZ = positionAttr.z - 1;
      cube.entity.setAttribute("position", { x: updatedX, y: updatedY, z: updatedZ });
    }
  }
}

function deleteAllCubeEntities(scene) {

  // Loop through each cube entity in the array
  for (var i = 0; i < cubeEntities.length; i++) {
    var cube = cubeEntities[i];

    // Remove the cube entity from the scene
    scene.removeChild(cube.entity);
  }

  // Clear the cubeEntities array
  cubeEntities = [];
}

AFRAME.registerComponent('scene-updater', {
  init: function () {
    this.sceneEl = document.querySelector("a-scene");
    this.skyEl = document.querySelector('a-sky');
  },

  // update: function () {
  //   let currentTime = new Date();
  //   let timeDifference = currentTime.getTime() - previousTime.getTime();
  //   console.log(timeDifference);
  //   secondsElapsed = secondsElapsed + timeDifference
  //   console.log("seconds Elapsed :" + secondsElapsed)
  // }
  
  // time: amount of elapsed time in milliseconds
  // timeDelta: different in time between last tick() and current tick()
  tick: function (time, timeDelta) {
    // --- Trigger Conditions -- //
    // if (time > 2000 && !beep0Boolean){
    //   const colors = ['#ff0000', '#00ff00', '#0000ff'];
    //   const randomColor = colors[Math.floor(Math.random() * colors.length)];
    //   this.skyEl.setAttribute('material', 'color', randomColor);
    //   beep0Boolean = true
    // }
    // if (time > 1000 && !beep1Boolean){
    //   console.log("hi 1")
    //   transitionSkybox(this.skyEl, skyboxBeep1)
    //   beep1Boolean = true
    // }
    // if (time > 4000 && !transitionBeep1Beep2){
    //   console.log("hi 2")
    //   transitionSkybox(this.skyEl, skyboxBeep2)
    //   transitionBeep1Beep2 = true
    // }
    // if (time > 5000 && !beep2Boolean){
    //   this.skyEl.setAttribute("src", skyboxBeep2)
    //   beep2Boolean = true
    // }
    // Beeps
    if (time > beep1Timer && !beep1Boolean){
      this.skyEl.setAttribute("src", beep1Skybox)
      beep1Boolean = true
    }
    if (time > beep2Timer && !beep2Boolean){
      this.skyEl.setAttribute("src", beep2Skybox)
      beep2Boolean = true
    }
    if (time > beep3Timer && !beep3Boolean){
      this.skyEl.setAttribute("src", beep3Skybox)
      beep3Boolean = true
    }    
    if (time > beep3Timer && !beep3Boolean){
      this.skyEl.setAttribute("src", beep3Skybox)
      beep3Boolean = true
    }
    if (time > beep4Timer && !beep4Boolean){
      this.skyEl.setAttribute("src", beep4Skybox)
      beep4Boolean = true
    }

    // Portals
    if (time > portal1Timer && !portal1Boolean){
      this.skyEl.setAttribute("src", portal1Skybox)
      portal1Boolean = true
      booleanGenerateCubes = true
    }
    if (time > portal2Timer && !portal2Boolean){
      this.skyEl.setAttribute("src", portal2Skybox)
      portal2Boolean = true
    }
    if (time > portal3Timer && !portal3Boolean){
      this.skyEl.setAttribute("src", portal3Skybox)
      portal3Boolean = true
    }  
    if (time > portal3Timer && !portal3Boolean){
      this.skyEl.setAttribute("src", portal3Skybox)
      portal3Boolean = true
    }
    if (time > portal4Timer && !portal4Boolean){
      this.skyEl.setAttribute("src", portal4Skybox)
      portal4Boolean = true
      booleanGenerateCubes = false
    }

    // Object Generator
    if (booleanGenerateCubes){
      generateCubes(this.sceneEl, timeDelta)
    }
    else {
      deleteAllCubeEntities(this.sceneEl)
    }
  }
});
